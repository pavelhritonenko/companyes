﻿using System;

namespace CompanyEs.Contracts
{
    public sealed class CompanyNameSet : IEvent
    {
        public CompanyNameSet()
        {
        }

        public CompanyNameSet(string id, string name, long version, DateTime factDateUtc)
        {
            FactDateUtc = factDateUtc;
            Id = id;
            Name = name;
            Version = version;
            RegistrationDateTimeUtc = DateTime.UtcNow;
        }

        public string Name { get; set; }

        public DateTime FactDateUtc { get; set; }

        public DateTime RegistrationDateTimeUtc { get; set; }
        public string Id { get; set; }
        public long Version { get; set; }
    }
}