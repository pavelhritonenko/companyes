﻿using System;

namespace CompanyEs.Contracts
{
    public class CompanySnapshot : IEventHandler<CompanyNameSet>
    {
        private readonly DateTime _registeredTillDateTimeUtc;
        private readonly DateTime _factsTillDateTimeUtc;

        public DateTime FactDateTimeUtc { get; set; }
        public string Name { get; set; }

        public CompanySnapshot(DateTime registeredTillDateTimeUtc, DateTime factsTillDateTimeUtc)
        {
            _registeredTillDateTimeUtc = registeredTillDateTimeUtc;
            _factsTillDateTimeUtc = factsTillDateTimeUtc;
        }

        public void Handle(CompanyNameSet ev)
        {
            if (_factsTillDateTimeUtc < ev.FactDateUtc)
            {
                return;
            }

            if (_registeredTillDateTimeUtc < ev.RegistrationDateTimeUtc)
            {
                return;
            }

            if (FactDateTimeUtc > ev.FactDateUtc)
            {
                return;
            }

            FactDateTimeUtc = ev.FactDateUtc;
            Name = ev.Name;
        }
    }
}