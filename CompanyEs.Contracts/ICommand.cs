﻿namespace CompanyEs.Contracts
{
    public interface ICommand
    {
        string Id { get; }
    }
}