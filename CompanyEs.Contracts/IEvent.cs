﻿namespace CompanyEs.Contracts
{
    public interface IEvent
    {
        string Id { get; }

        long Version { get; set; }
    }
}