﻿using System.Collections.Generic;

namespace CompanyEs.Contracts
{
    public delegate void Handler<in T>(T msg);

    public interface IEventHandler<in T> where T : IEvent
    {
        void Handle(T ev);
    }

    public interface ICommandHandler<in T> where T : ICommand
    {
        IEnumerable<IEvent> Handle(T msg);
    }
}
