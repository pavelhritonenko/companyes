using System;

namespace CompanyEs.Contracts
{
    public sealed class SetCompanyName : ICommand
    {
        public SetCompanyName()
        {
        }

        public SetCompanyName(string id, string name, DateTime factDateTimeUtc)
        {
            Id = id;
            Name = name;
            FactDateTimeUtc = factDateTimeUtc;
        }

        public string Name { get; set; }
        public string Id { get; set; }

        public DateTime FactDateTimeUtc { get; set; }
    }
}