﻿using System;
using System.Net.Http;
using System.Threading;
using CompanyEs.Contracts;
using FluentAssertions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NUnit.Framework;

namespace CompanyEs.Tests
{
    [TestFixture]
    public class Scenario
    {
        private string _companyId = "company-" + Guid.NewGuid();

        private readonly HttpClient _client;

        public Scenario()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("http://localhost/CompanyEs/api/company/");
        }

        [Test]
        public void set_then_get()
        {
            SetName("name 1", Date(01, 06));
            GetActual().Name.Should().Be("name 1");
        }

        [Test]
        public void double_set_then_get()
        {
            SetName("name 1", Date(01, 06));

            // Thread.Sleep(1000);

            // var timeStamp = DateTime.UtcNow;

            SetName("name 2", Date(02, 06));

            GetActual().Name.Should().Be("name 2");
        }

        [Test]
        public void reverse_order_set_then_get()
        {
            SetName("name 1", Date(02, 06));

            // Thread.Sleep(1000);

            // var timeStamp = DateTime.UtcNow;

            SetName("name 2", Date(01, 06));

            GetActual().Name.Should().Be("name 1");
        }

        [Test]
        public void prev_snapshot_order_set_then_get()
        {
            SetName("name 1", Date(02, 06));

            Thread.Sleep(1000);

            var timeStamp = DateTime.UtcNow;

            Thread.Sleep(1000);

            SetName("name 2", Date(01, 06));

            GetSnapshot(timeStamp).Name.Should().Be("name 1");
        }

        private CompanySnapshot GetSnapshot(DateTime timeStamp)
        {
            var response = _client.GetAsync("snapshot/" + _companyId +
                "?actualDateTimeUtc=" + ToUri(timeStamp) + "&" +
                "factsDateTimeUtc=" + ToUri(DateTime.UtcNow)).Result;
            response.EnsureSuccessStatusCode();

            return response.Content.ReadAsAsync<CompanySnapshot>().Result;
        }

        private string ToUri(DateTime timeStamp)
        {
            return String.Format("{0}-{1}-{2}T{3}:{4}:{5}.{6}Z",
                timeStamp.Year,
                timeStamp.Month,
                timeStamp.Day,
                timeStamp.Hour,
                timeStamp.Minute,
                timeStamp.Second,
                timeStamp.Millisecond);
        }

        private CompanySnapshot GetActual()
        {
            var response = _client.GetAsync("actual/" + _companyId).Result;
            response.EnsureSuccessStatusCode();

            return response.Content.ReadAsAsync<CompanySnapshot>().Result;
        }

        private DateTime Date(int day, int month)
        {
            return DateTime.SpecifyKind(new DateTime(2014, month, day), DateTimeKind.Utc);
        }

        public void SetName(string name, DateTime factUtc)
        {
            var setCompanyName = new SetCompanyName(_companyId, name, factUtc);
            _client.PostAsJsonAsync("setname/" + _companyId, setCompanyName).Wait();
        }
    }
}
