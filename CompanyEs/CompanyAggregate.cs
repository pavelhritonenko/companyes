﻿using System.Collections.Generic;
using CompanyEs.Contracts;

namespace CompanyEs
{
    public class CompanyState
    {
        public long Version { get; private set; }

        public void When(CompanyNameSet ev)
        {
            Version += 1;
        }
    }

    public class CompanyAggregate : ICommandHandler<SetCompanyName>
    {
        private readonly CompanyState _state;

        public CompanyAggregate(CompanyState state)
        {
            _state = state;
        }

        public IEnumerable<IEvent> Handle(SetCompanyName msg)
        {
            yield return new CompanyNameSet(msg.Id, msg.Name, _state.Version, msg.FactDateTimeUtc);
        }
    }


}