﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using CompanyEs.Contracts;

namespace CompanyEs.Controllers
{
    public class CompanyController : ApiController
    {
        Repository _repository = new Repository();

        [HttpGet]
        public string Index()
        {
            return "blah blah blah";
        }

        [HttpPost]
        public async Task SetName(SetCompanyName request)
        {
            await Execute(request);
        }

        [HttpGet]
        public async Task<CompanySnapshot> Snapshot(string id, DateTime actualDateTimeUtc, DateTime factsDateTimeUtc)
        {
            var companySnapshot = new CompanySnapshot(actualDateTimeUtc, factsDateTimeUtc);

            await Apply(companySnapshot, id);

            return companySnapshot;
        }

        [HttpGet]
        public async Task<CompanySnapshot> Actual(string id)
        {
            var snapshot = new CompanySnapshot(DateTime.UtcNow, DateTime.UtcNow);

            await Apply(snapshot, id);

            return snapshot;
        }

        private async Task Apply(CompanySnapshot companySnapshot, string id)
        {
            var events = await _repository.GetById(id);

            foreach (var ev in events)
            {
                companySnapshot.Handle((dynamic)ev);
            }
        }

        public async Task Execute<T>(T command) where T : ICommand
        {
            var companyState = new CompanyState();
            var aggregate = new CompanyAggregate(companyState);

            var events = await _repository.GetById(command.Id);

            foreach (var ev in events)
            {
                companyState.When((dynamic)ev);
            }

            var expectedVersion = companyState.Version;

            var changes = (aggregate as ICommandHandler<T>).Handle(command);

            await _repository.SaveAsync(command.Id, changes, expectedVersion);
        }
    }
}