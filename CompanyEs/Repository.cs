﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CompanyEs.Contracts;
using Newtonsoft.Json;

namespace CompanyEs
{
    public class Repository
    {
        private readonly JsonSerializerSettings _settings;


        public Repository()
        {
            _settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
        }

        public async Task<IEnumerable<IEvent>> GetById(string id)
        {
            return File.ReadAllLines(GetFileName(id)).Select(Deserialize);
        }

        private IEvent Deserialize(string line, int index)
        {
            return JsonConvert.DeserializeObject(line, typeof(IEvent), _settings) as IEvent;
        }

        private string GetFileName(string id)
        {
            // плохо, надо вынести в отдельную функцию
            var fileName = String.Format("c:/data/{0}.json", id);

            if (!File.Exists(fileName))
            {
                using (File.CreateText(fileName))
                {
                }
            }

            return fileName;
        }

        public async Task SaveAsync(string id, IEnumerable<IEvent> events, long expectedVersion)
        {
            File.AppendAllLines(GetFileName(id), events.Select(Serialize));
        }

        private string Serialize(IEvent @event)
        {
            return JsonConvert.SerializeObject(@event, _settings);
        }
    }
}